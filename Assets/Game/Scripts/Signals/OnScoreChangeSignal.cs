﻿using Game.Scripts.Enums;

public struct OnScoreChangeSignal
{
    public ScoreSignalType ScoreSignalType;
    public int Value;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Spawner : MonoBehaviour
{
    BlueEnemyFacade.BlueEnemyPool _bluePool;
    RedEnemyFacade.RedEnemyPool _redPool;
    AEnemy.AEnemyPool[] _enemyPools;
    Vector3[] spawnPoints = new Vector3[5];

    PlayerFacade.PlayerFactory _playerFactory;



    [Inject] private void Construct(PlayerFacade.PlayerFactory playerFactory, BlueEnemyFacade.BlueEnemyPool bluePool, RedEnemyFacade.RedEnemyPool redPool)
    {
        _bluePool = bluePool;
        _redPool = redPool;
        _enemyPools = new AEnemy.AEnemyPool[2] { _bluePool, _redPool };

        for (int i = 1; i <= 5; i++)
        {
            Vector3 section = new Vector3(Screen.width / 5 * i, Screen.height * 1.2f, 1);
            spawnPoints[i - 1] = Camera.main.ScreenToWorldPoint(section);
        }

        _playerFactory = playerFactory;

        InvokeRepeating("Spawn", 3, 3);
    }

    private void Start()
    {
        _playerFactory.Create();
    }

    private void Spawn()
    {
        int i = Random.Range(0, 2);
        int j = Random.Range(0, 5);
        _enemyPools[i].Spawn(spawnPoints[j], _enemyPools[i]);
    }
}
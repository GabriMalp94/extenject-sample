﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ShotSystem
{
    private BulletFacade.BulletPool _bulletPool;

    [Inject] private void Construct(BulletFacade.BulletPool bulletPool)
    {
        _bulletPool = bulletPool;
    }

    public void Shot(IShooter shooter)
    {
        shooter.MuzzleFlash.Play();
        _bulletPool.Spawn(shooter.BulletInfo, _bulletPool);
    }
}

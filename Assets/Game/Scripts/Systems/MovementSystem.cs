﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementSystem
{
    public void Move(IMovable movable)
    {
        if (movable.Direction.sqrMagnitude > 0)
        {
            Vector3 pos = movable.Transform.position + movable.Direction * Time.deltaTime * movable.Speed;
         
            pos.x = Mathf.Clamp(pos.x, movable.MinX, movable.MaxX);
            movable.Transform.position = pos;
        }
    }
}
﻿using System;
using Game.Scripts.Enums;
using Zenject;

namespace Game.Scripts.Systems
{
    public class ScoreSystem : IInitializable, IDisposable
    {
        private GameData _gameData;
        private SignalBus _signalBus;

        public int Score
        {
            get => _gameData.Score;
            private set => _gameData.Score = value;
        }
        
        public ScoreSystem(GameData gameData, SignalBus signalBus)
        {
            _gameData = gameData;
            _signalBus = signalBus;
        }

        public void Initialize() =>_signalBus.Subscribe<OnScoreChangeSignal>(OnScoreChangeSignal);

        public void Dispose() => _signalBus.Unsubscribe<OnScoreChangeSignal>(OnScoreChangeSignal);
            

        private void OnScoreChangeSignal(OnScoreChangeSignal signal)
        {
            switch (signal.ScoreSignalType)
            {
                case ScoreSignalType.Reset:
                    Score = 0;
                    break;
                case ScoreSignalType.Modify:
                    Score += signal.Value;
                    break;
            }
        }
    }
}
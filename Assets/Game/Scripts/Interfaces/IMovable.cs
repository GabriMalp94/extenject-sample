﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovable
{
  Vector3 Direction { get; }
    Transform Transform { get; }
    float Speed { get; }
    float MinX { get; }
    float MaxX { get; }
}

﻿using UnityEngine;

public interface IShooter
{
    BulletInfo BulletInfo { get; }
    ParticleSystem MuzzleFlash { get; }
}
public struct BulletInfo
{
    public int damage;
    public Vector3 direction;
    public Vector3 startPosition;
    public float speed;
    public string layer;
}
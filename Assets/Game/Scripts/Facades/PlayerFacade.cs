﻿using System;
using System.Collections;
using Game.Scripts.Enums;
using UnityEngine;
using UnityEngine.InputSystem;
using Zenject;

public class PlayerFacade : MonoBehaviour, IMovable, IShooter, IDamageable
{
    private PlayerData _data;
    private MovementSystem _movementSystem;
    private ShotSystem _shotSystem;

    private Vector3 direction;

    public Vector3 Direction => direction;

    public Transform Transform => transform;

    public float Speed => _settings.MovementSpeed;

    private BulletInfo bulletInfo;
    public BulletInfo BulletInfo { get => bulletInfo; private set => bulletInfo = value; }
    public float MinX { get; private set; }

    public float MaxX { get; private set; }

   [Inject] public ParticleSystem MuzzleFlash { get; private set; } 

    private PlayerSettings _settings;

    private Camera _camera;
    private SignalBus _signalBus;


    [Inject]
    private void Construct(MovementSystem movementSystem, ShotSystem shotSystem, PlayerSettings settings, Camera camera, PlayerData data, SignalBus signalBus)
    {
        _data = data;
        _settings = settings;
        _movementSystem = movementSystem;
        _shotSystem = shotSystem;
        _camera = camera;
        _signalBus = signalBus;
    }

    private void Start()
    {
        _data.HP = _settings.defaultMaxHP;
        _data.Damage = _settings.defaultDamage;
        bulletInfo.direction = Vector3.up;
        bulletInfo.speed = _settings.BulletSpeed;
        bulletInfo.layer = _settings.BulletLayer;
        MinX = _camera.ScreenToWorldPoint(Vector3.zero).x + transform.localScale.x / 2;
        MaxX = _camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x - transform.localScale.x / 2;
    }

    private IEnumerator Shot()
    {
        while(true)
        {
            if(Time.frameCount % _settings.FireInterval_fps == 0)
            {
                bulletInfo.startPosition = transform.position;
                bulletInfo.damage = _data.Damage;
                _shotSystem.Shot(this);
            }
            yield return null;
        }
    }

    private IEnumerator Move()
    {
        while (true)
        {
            _movementSystem.Move(this);
            yield return null;
        }
    }



    public void OnMove(InputAction.CallbackContext context)
    {
        direction.x = context.ReadValue<Vector2>().x;
        if (context.performed) StartCoroutine("Move");
        if (context.canceled) StopCoroutine("Move");

    }
    
    public void OnShot(InputAction.CallbackContext context)
    {
        if (context.performed) StartCoroutine("Shot");
        if (context.canceled) StopCoroutine("Shot");
    }

    public void Damage(int damage)
    {
        _data.HP -= damage;
        if (_data.HP <= 0)
        {
            _data.HP = _settings.defaultMaxHP;
            _signalBus.Fire(new  OnScoreChangeSignal() {ScoreSignalType = ScoreSignalType.Reset});
        }
    }


    public class PlayerFactory : PlaceholderFactory<PlayerFacade>
    {
        
    }
}
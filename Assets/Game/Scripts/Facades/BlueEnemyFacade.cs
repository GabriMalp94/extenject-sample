﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BlueEnemyFacade : AEnemy
{

    protected override EnemySettings Settings => _blueSettings;
      private BlueEnemySettings _blueSettings;
    public override float Speed => _blueSettings.MoveSpeed;
    [Inject]
    private void SettingInject(BlueEnemySettings blueEnemySettings)
    {
        _blueSettings = blueEnemySettings;
    }

    public class BlueEnemyPool : AEnemyPool
    {
        
    }
}

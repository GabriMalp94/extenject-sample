﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System;
using Game.Scripts.Enums;

public abstract class AEnemy : MonoBehaviour, IMovable, IDisposable, IDamageable
{
    private EnemyData _data;
    //protected EnemySettings _settings;

    protected virtual EnemySettings Settings { get; set; }

    private MovementSystem _movementSystem;

    
    protected BulletInfo bulletInfo;
    public BulletInfo BulletInfo => bulletInfo;
    public Vector3 Direction => Vector3.down;

    public Transform Transform => transform;

    public abstract float Speed { get; }

    private IMemoryPool _memoryPool;

    public float MinX { get; private set; }

    public float MaxX { get; private set; }
    private Camera _camera;
    private SignalBus _signalBus;

    [Inject] private void Construct(MovementSystem movementSystem, Camera camera, EnemyData data, SignalBus signalBus)
    {
        _movementSystem = movementSystem;
        _camera = camera;
        _data = data;
        _signalBus = signalBus;

    }
    private void Start()
    {
        MinX = _camera.ScreenToWorldPoint(Vector3.zero).x + transform.localScale.x / 2;
        MaxX = _camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x - transform.localScale.x / 2;
    }

    protected virtual void Update()
    {
        _movementSystem.Move(this);
    }
    private void OnBecameInvisible()
    {
        if (_data.HP > 0)
        {
            _signalBus.Fire(new OnScoreChangeSignal(){ScoreSignalType = ScoreSignalType.Reset});
        }
        Dispose();
    }
    public void Dispose()
    {
        _memoryPool?.Despawn(this);
    }

    public void Damage(int damage)
    {
        _data.HP -= damage;
        if(_data.HP <= 0)
        {
            _signalBus.Fire(new  OnScoreChangeSignal() {ScoreSignalType = ScoreSignalType.Modify, Value = 1});
            Dispose();
        }
    }

    public class AEnemyPool : MonoMemoryPool<Vector3, IMemoryPool, AEnemy> 
    {
        protected override void Reinitialize(Vector3 p1, IMemoryPool memoryPool, AEnemy item)
        {
            item.transform.position = p1;
            item._memoryPool = memoryPool;
            item._data.HP = 3;
        }
        protected override void OnDespawned(AEnemy item)
        {
            item._memoryPool = null;
            base.OnDespawned(item);
        }
    }
}

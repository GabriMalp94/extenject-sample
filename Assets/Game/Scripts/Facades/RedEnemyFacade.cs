﻿using UnityEngine;
using Zenject;

public class RedEnemyFacade : AEnemy, IShooter
{
    public override float Speed => _redSettings.MoveSpeed;
    private RedEnemySettings _redSettings;
    protected override EnemySettings Settings => _redSettings;
    private ShotSystem _shotSystem;

    [Inject] public ParticleSystem MuzzleFlash { get; private set; }

    [Inject]
    private void InjectSettings(RedEnemySettings redEnemySettings)
    {
        _redSettings = redEnemySettings;
    }
    [Inject] private void InjectSystems(ShotSystem shotSystem)
    {
        _shotSystem = shotSystem;
    }
    private void Start()
    {
        bulletInfo.direction = Vector3.down;
        bulletInfo.speed = _redSettings.BulletSpeed;
        bulletInfo.damage = _redSettings.Damage;
        bulletInfo.layer = _redSettings.BulletLayer;
    }

    protected override void Update()
    {
        base.Update();
        if(Time.frameCount % _redSettings.FireInterval_fps == 0)
        {
            bulletInfo.startPosition = transform.position;
            _shotSystem.Shot(this);
        }
    }
    public class RedEnemyPool : AEnemyPool
    {
    }
}

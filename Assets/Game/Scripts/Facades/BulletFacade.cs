﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Zenject;
using System;
public class BulletFacade : MonoBehaviour, IMovable, IDisposable
{
    private BulletInfo _bulletInfo;

    private MovementSystem _movementSystem;

    public Vector3 Direction => _bulletInfo.direction;

    public Transform Transform => transform;

    public float Speed => _bulletInfo.speed;

    private IMemoryPool _memoryPool;

    public float MinX { get; private set; }

    public float MaxX { get; private set; }
    private Camera _camera;

    [Inject] private void Construct(MovementSystem movementSystem, Camera camera)
    {
        _movementSystem = movementSystem;
        _camera = camera;
       
    }
    private void Start()
    {
        MinX = _camera.ScreenToWorldPoint(Vector3.zero).x + transform.localScale.x / 2;
        MaxX = _camera.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x - transform.localScale.x / 2;
    }
    private void OnBecameInvisible() => Dispose();

  
    private void Update()
    {
        _movementSystem.Move(this);
    }

    public void Dispose()
    {
        _memoryPool?.Despawn(this);
    }

    private void OnTriggerEnter(Collider other)
    {
        other.GetComponentInParent<IDamageable>()?.Damage(_bulletInfo.damage);
        Dispose();
    }

    public class BulletPool : MonoMemoryPool<BulletInfo, IMemoryPool, BulletFacade>
    {
        protected override void Reinitialize(BulletInfo info, IMemoryPool memoryPool, BulletFacade item)
        {
            item._memoryPool = memoryPool;
            item._bulletInfo = info;
            item.transform.position = info.startPosition;
            item.gameObject.layer = LayerMask.NameToLayer(info.layer);
        }

        protected override void OnDespawned(BulletFacade item)
        {
            item._memoryPool = null;
            base.OnDespawned(item);
        }
    }
}

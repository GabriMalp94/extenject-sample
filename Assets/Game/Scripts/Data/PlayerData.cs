﻿
public class PlayerData
{
    private int hp;
    private int damage;
    private HUD _hud;
    public PlayerData(HUD hud)
    {
        _hud = hud;
    }
    
    public int HP
    {
        get => hp;
        set
        {
            hp = value;
            _hud._hp.text = $"Player HP: {hp}";
        }
    }
    public int Damage
    {
        get => damage;
        set => damage = value;
    }
}

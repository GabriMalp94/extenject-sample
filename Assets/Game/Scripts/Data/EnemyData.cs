﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


    public class EnemyData
    {
        private int hp;
        private int damage;
        private HUD _hud;
        public EnemyData(HUD hud)
        {
            _hud = hud;
        }
        
        public int HP
        {
            get => hp;
            set => hp = value;
        }
        public int Damage
        {
            get => damage;
            set => damage = value;
        }
    }


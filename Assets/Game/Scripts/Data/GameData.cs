﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData
{
    public GameData(HUD hud)
    {
        _hud = hud;
    }

    private HUD _hud;
    private int _score;

    public int Score
    {
        get => _score;
        set
        {
            _score = value;
            _hud._score.text = $"score: {_score}";
        }
    }
}

using Game.Scripts.Installers;
using UnityEngine;
using Zenject;

public class SceneGameplayInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        SignalBusInstaller.Install(Container);
        SignalInstaller.Install(Container);
        DataInstaller.Install(Container);
        SettingsInstaller.Install(Container);
        SystemInstaller.Install(Container);
        FactoriesInstaller.Install(Container);
        PoolInstaller.Install(Container);
        HierarchyInstaller.Install(Container);
        
    }
}
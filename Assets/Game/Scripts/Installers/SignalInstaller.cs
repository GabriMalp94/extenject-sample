﻿using Zenject;

namespace Game.Scripts.Installers
{
    public class SignalInstaller : Installer<SignalInstaller>
    {
        public override void InstallBindings()
        {
            Container.DeclareSignal<OnScoreChangeSignal>();
        }
    }
}
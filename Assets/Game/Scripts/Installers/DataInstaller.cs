using UnityEngine;
using Zenject;

public class DataInstaller : Installer<DataInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<PlayerData>().FromNew().AsSingle();
        Container.Bind<EnemyData>().FromNew().AsTransient();
        Container.Bind<GameData>().FromNew().AsSingle();
    }
}
using UnityEngine;
using Zenject;

public class SettingsInstaller : Installer<SettingsInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<PlayerSettings>().FromScriptableObjectResource("PlayerSettings").AsSingle();
        Container.Bind<BlueEnemySettings>().FromScriptableObjectResource("BlueEnemySettings").AsSingle();
        Container.Bind<RedEnemySettings>().FromScriptableObjectResource("RedEnemySettings").AsSingle();
    }
}
using UnityEngine;
using Zenject;

public class FactoriesInstaller : Installer<FactoriesInstaller>
{
    public override void InstallBindings()
    {
        Container
           .BindFactory<PlayerFacade, PlayerFacade.PlayerFactory>()
           .FromComponentInNewPrefabResource("Player");
    }
}
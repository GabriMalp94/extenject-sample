using Game.Scripts.Systems;
using UnityEngine;
using Zenject;

public class SystemInstaller : Installer<SystemInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<MovementSystem>().AsSingle();
        Container.Bind<ShotSystem>().AsSingle();
        Container.BindInterfacesTo<ScoreSystem>().AsSingle();
    }
}
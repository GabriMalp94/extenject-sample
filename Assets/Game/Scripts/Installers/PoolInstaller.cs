using UnityEngine;
using Zenject;

public class PoolInstaller : Installer<PoolInstaller>
{
    public override void InstallBindings()
    {
        Container
           .BindMemoryPool<BulletFacade, BulletFacade.BulletPool>()
           .WithInitialSize(3)
           .FromComponentInNewPrefabResource("Bullet");
        Container
            .BindMemoryPool<BlueEnemyFacade, BlueEnemyFacade.BlueEnemyPool>()
            .FromComponentInNewPrefabResource("BlueEnemy");
        Container
        .BindMemoryPool<RedEnemyFacade, RedEnemyFacade.RedEnemyPool>()
        .FromComponentInNewPrefabResource("RedEnemy");
    }
}
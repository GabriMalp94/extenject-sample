using UnityEngine;
using Zenject;

public class HierarchyInstaller : Installer<HierarchyInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<Camera>().FromComponentInHierarchy().AsSingle();
        Container.Bind<HUD>().FromComponentInHierarchy().AsSingle();

        //Container.Bind<ParticleSystem>().FromComponentsInChildren().AsSingle();
    }
}
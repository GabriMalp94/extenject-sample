﻿using UnityEngine;

[CreateAssetMenu(fileName ="PlayerSettings", menuName = "Settings/PlayerSettings")]
public class PlayerSettings : ScriptableObject
{
    public float MovementSpeed;
    public int FireInterval_fps;
    public float BulletSpeed;
    public int defaultMaxHP;
    public int defaultDamage;
    public string BulletLayer;
}
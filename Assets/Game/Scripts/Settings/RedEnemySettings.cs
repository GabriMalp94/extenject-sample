﻿using UnityEngine;

[CreateAssetMenu(fileName = "RedEnemySettings", menuName = "Settings/Enemies/RedEnemySettings")]
public class RedEnemySettings : EnemySettings
{
    public string BulletLayer;
    public float BulletSpeed;
    public int FireInterval_fps;
    public int Damage;
}
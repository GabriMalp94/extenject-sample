﻿namespace Game.Scripts.Enums
{
    public enum ScoreSignalType
    {
        Reset,
        Modify,
    }
}